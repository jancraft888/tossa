![tossa logo](./tossa.png)

# tossa
using your Android device as a gaming center

## tossa-python features
 - USB connection
 - true mouse and keyboard support
 - only linux, mac & windows
 - extra low latency
 - configurable

## tossa-web features
 - web interface
 - Chrome-only support
 - USB connection
 - keymapping support
 - low latency
 - emulated mouse and keyboard
 - (relatively) easy to use
*(sadly, I can't get it to work on iOS devices due to lack of USB API support)*

You can access tossa web [here](https://tossa.vercel.app/). I will be upgrading tossa-web so it supports better controls soon!
