#!/bin/sh

python3 -c 'import sys; exit(not (sys.version_info.major >= 3 and sys.version_info.minor >= 10))'
if [ $? -eq 0 ]; then
  echo "check: python 3.10 or above"
else
  echo "fail: python 3.10 or above is needed"
  exit
fi


