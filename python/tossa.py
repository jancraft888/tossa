#!/usr/bin/python3
# tossa - your android device as a gaming center

#from pynput.mouse import Button, Controller, Listener
#from pynput.keyboard import Key, Controller, Listener
from configparser import ConfigParser
from subprocess import run as srun
from os.path import exists
from ast import literal_eval as seval
import urllib.request

scrcpy_config = { 'width': '1024', 'bitrate': '8', 'targetfps': '30', 'truekeyboard': 'True', 'truemouse': 'False' }
tossa_config = { 'keymapping': 'False' }

configfile = ConfigParser()
if exists('tossa.cfg'):
    configfile.read('tossa.cfg')
    for opt in configfile.options('scrcpy'):
        scrcpy_config[opt] = configfile.get('scrcpy', opt)
    for opt in configfile.options('tossa'):
        tossa_config[opt] = configfile.get('tossa', opt)
else:
    configfile.add_section('scrcpy')
    for k, v in scrcpy_config.items():
        configfile.set('scrcpy', k, v)
    configfile.add_section('tossa')
    for k, v in tossa_config.items():
        configfile.set('tossa', k, v)
    with open('tossa.cfg', 'w') as f:
        configfile.write(f)

if __name__ == '__main__':
    print("tossa 1.0 <https://gitlab.com/jancraft888/tossa>")

    args = [ 'scrcpy', '--window-title', 'tossa', '-Sw', '-m', scrcpy_config['width'], '-b', scrcpy_config['bitrate'], '--max-fps', scrcpy_config['targetfps'] ]
    if seval(scrcpy_config['truekeyboard']): args.append('-K')
    if seval(scrcpy_config['truemouse']):
        args.append('-M')
        args.append('--forward-all-clicks')
    if str(tossa_config['keymapping']) == 'True':
        urllib.request.urlretrieve("https://github.com/keymapperorg/KeyMapper/releases/download/v2.4.4/keymapper-2.4.4.apk", "keymapper.tmp.apk")
        srun([ 'adb', 'install', 'keymapper.tmp.apk' ])
    srun(args)
