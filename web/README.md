# tossa-web
this is based on [yume-chan/ya-webadb](https://github.com/yume-chan/ya-webadb), it really only modifies it slightly so it can use all the keymapping features and removes all unnecessary stuff.


## Credits

* yume-chan for [ya-webadb](https://github.com/yume-chan/ya-webadb) ([MIT License](./LICENSE))
* Google for [ADB](https://android.googlesource.com/platform/packages/modules/adb) ([Apache License 2.0](./adb.NOTICE))
* Romain Vimont for [Scrcpy](https://github.com/Genymobile/scrcpy) ([Apache License 2.0](https://github.com/Genymobile/scrcpy/blob/master/LICENSE))
