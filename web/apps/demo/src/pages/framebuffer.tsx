import { ICommandBarItemProps, Stack } from '@fluentui/react';
import { AdbFrameBuffer } from "@yume-chan/adb";
import { action, autorun, computed, makeAutoObservable } from "mobx";
import { observer } from "mobx-react-lite";
import { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useRef } from 'react';
import { CommandBar, DemoModePanel, DeviceView } from '../components';
import { GlobalState } from "../state";
import { Icons, RouteStackProps } from "../utils";

class FrameBufferState {
    width = 0;
    height = 0;
    imageData: ImageData | undefined = undefined;
    imageBitmap: ImageBitmap | undefined = undefined;

    keymapfile: { [key: string]: number[] } = {};

    constructor() {
        makeAutoObservable(this);
      }

    async setImage(image: AdbFrameBuffer) {
        this.width = image.width;
        this.height = image.height;
        this.imageData = new ImageData(new Uint8ClampedArray(image.data), image.width, image.height);
        this.imageBitmap = await createImageBitmap(this.imageData);
    }
}

const state = new FrameBufferState();

const FrameBuffer: NextPage = (): JSX.Element | null => {
    const canvasRef = useRef<HTMLCanvasElement | null>(null);

    const capture = useCallback(async () => {
        if (!GlobalState.device) {
            return;
        }

        try {
            const framebuffer = await GlobalState.device.framebuffer();
            state.setImage(framebuffer);
        } catch (e: any) {
            GlobalState.showErrorDialog(e);
        }
    }, []);

    
    const renderCircle = (ctx: CanvasRenderingContext2D, c: string, x: number, y: number, r: number) => {
        ctx.fillStyle = c;
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI);
        ctx.fill();
    }
    const renderText = (ctx: CanvasRenderingContext2D, str: string, x: number, y: number) => {
        ctx.fillStyle = '#000';
        ctx.font = 'bold 24px serif';
        ctx.fillText(str, x, y);
    }

    useEffect(() => {
        return autorun(() => {
            const canvas = canvasRef.current;
            if (canvas && state.imageData) {
                canvas.width = state.width;
                canvas.height = state.height;
                const context = canvas.getContext("2d")!;
                context.drawImage(state.imageBitmap!, 0, 0);
                for (const key in state.keymapfile) {
                    const pt = state.keymapfile[key]
                    let col;
                    switch (key) {
                        case '__grabpointer':
                          col = '#afa';
                          break;
                        case '__leftclick':
                          col = '#faa';
                          break;
                        case '__rightclick':
                          col = '#aaf';
                          break;
                        default:
                          col = '#ffe'
                    }
                    renderCircle(context, col, pt[0] * state.width, pt[1] * state.height, 0.025 * Math.min(state.width, state.height));
                    renderText(context, key, pt[0] * state.width, pt[1] * state.height);
                }
            }
        });
    }, []);

    const commandBarItems = computed(() => [
        {
            key: 'start',
            disabled: !GlobalState.device,
            iconProps: { iconName: Icons.Camera, style: { height: 20, fontSize: 20, lineHeight: 1.5 } },
            text: 'Capture',
            onClick: capture as VoidFunction,
        }
    ] as ICommandBarItemProps[]);

    const commandBarFarItems = computed((): ICommandBarItemProps[] => [
        {
            key: 'Load',
            iconProps: { iconName: Icons.Folder },
            checked: false,
            text: 'Load',
            onClick: action(() => {
                const inp = document.createElement('input')
                inp.type = 'file'
                inp.accept = '.tkf,.json,application/tossa,application/json'
                inp.onchange = async () => {
                    const f = await inp.files?.item(0)?.text()
                    if (!f) return
                    const keyf = JSON.parse(f)
                    console.log(keyf)
                    state.keymapfile = keyf
                }
                inp.click()

            })
        },
        {
            key: 'Save',
            iconProps: { iconName: Icons.Save },
            checked: false,
            text: 'Save',
            onClick: action(() => {
                const a = document.createElement('a');
                a.download = 'keymap.tkf';
                a.href = URL.createObjectURL(new Blob([JSON.stringify(state.keymapfile)], {type : 'application/tossa'}));
              a.click();
            }),
        },
        {
            key: 'info',
            iconProps: { iconName: Icons.Info, style: { height: 20, fontSize: 20, lineHeight: 1.5 } },
            iconOnly: true,
            tooltipHostProps: {
                content: 'Take a screenshot and place keymaps on top of it.',
                calloutProps: {
                    calloutMaxWidth: 250,
                }
            },
        }
    ]);

    const clamp = (x: number, m: number, M: number) => Math.max(Math.min(x, M), m);

    const calculatePointerPosition = (clientX: number, clientY: number) => {
        const viewRect =  canvasRef.current!.getBoundingClientRect();
        let pointerViewX = clamp((clientX - viewRect.x) / viewRect.width, 0, 1);
        let pointerViewY = clamp((clientY - viewRect.y) / viewRect.height, 0, 1);

        return {
            x: pointerViewX * state.width,
            y: pointerViewY * state.height,
        };
    }

    let ptrpos = [ 0.5, 0.5 ];
    const handleKeyDown = async (e: React.KeyboardEvent<HTMLCanvasElement>) => {
        if (e.code == 'ControlLeft' || e.code == 'AltLeft') return;
        if (e.code == 'Digit1' && e.ctrlKey) {
            state.keymapfile['__leftclick'] = [ ptrpos[0], ptrpos[1] ]
        } else if (e.code == 'Digit2' && e.ctrlKey) {
            state.keymapfile['__rightclick'] = [ ptrpos[0], ptrpos[1] ]
        } else {
            state.keymapfile[e.code] = [ ptrpos[0], ptrpos[1] ]
        }
    };
    const handlePointerDown = async (e: React.PointerEvent<HTMLCanvasElement>) => {
        if (e.button == 2) {
          for (const keybind in state.keymapfile) {
              const pos = state.keymapfile[keybind]
              const diff = [ ptrpos[0] - pos[0], ptrpos[1] - pos[1] ]
              const dst = Math.sqrt(diff[0] * diff[0] + diff[1] * diff[1])
              console.log(dst)
              if (dst < 0.025) {
                  delete state.keymapfile[keybind]
                  break;
              }
          }
          e.preventDefault();
        }
        else if (e.button == 1) state.keymapfile['__grabpointer'] = [ ptrpos[0], ptrpos[1] ]
    };
    const handlePointerMove = async (e: React.PointerEvent<HTMLCanvasElement>) => {
        const c = calculatePointerPosition(e.clientX, e.clientY)
        ptrpos = [ c.x / state.width, c.y / state.height ]
    };

    return (
        <Stack {...RouteStackProps}>
            <Head>
                <title>Tossa Keymapper</title>
            </Head>

            <CommandBar items={commandBarItems.get()} farItems={commandBarFarItems.get()} />
            <Stack horizontal grow styles={{ root: { height: 0 } }}>
                <DeviceView width={state.width} height={state.height}>
                    <canvas tabIndex={0} ref={canvasRef} onPointerMove={handlePointerMove} onPointerDown={handlePointerDown} onKeyDown={handleKeyDown} style={{ display: 'block' }} />
                </DeviceView>
            </Stack>
        </Stack>
    );
};

export default observer(FrameBuffer);
